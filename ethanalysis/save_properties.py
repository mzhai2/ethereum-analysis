from pymongo import MongoClient, IndexModel, UpdateOne, TEXT, ASCENDING
import pymongo
from tqdm import tqdm
from decimal import Decimal
from bson.decimal128 import Decimal128

# values transferred between parties
def write_pairwise_values(transactions, pairwise, start, end):
    match = {'$match':
                {'$and':[
                    {'blockNumber': {'$gte':start}},
                    {'blockNumber': {'$lt':end}},
                ]}
            }
    # accumulate the value of all the to and from tuples
    for i, r in enumerate(tqdm(transactions.aggregate([match]))):
        pairwise.update_one({'_id': '{}_{}'.format(r['from'], r['to'])}, {'$inc': {'value': r['value']}}, upsert=True)

# calculates the number of unique counterparties
# for to, from, both
def write_counterparties(transactions, counterparties, start, end):
    match = {'$match':
                {'$and':[
                    {'blockNumber': {'$gte': start}},
                    {'blockNumber': {'$lt': end}},
                ]}
            }
    for i, r in enumerate(tqdm(transactions.aggregate([match]))):
        reqs = [UpdateOne({'_id': r['to']}, {'$addToSet': {'received_from': [r['from']]}}, upsert=True),
            UpdateOne({'_id': r['from']}, {'$addToSet': {'sent_to': [r['to']]}}, upsert=True),
            UpdateOne({'_id': r['to']}, {'$addToSet': {'any': [r['to'], r['from']]}}, upsert=True),
            UpdateOne({'_id': r['from']}, {'$addToSet': {'any': [r['to'], r['from']]}}, upsert=True)]

        counterparties.bulk_write(reqs, ordered=False) 

def write_counterparties_split(transactions, counterparties, start, end):
    step = (end-start)//100
    for start in tqdm(range(start, end, step)):
        end = start+step
        match = {'$match':
                    {'$and':[
                        {'blockNumber': {'$gte': start}},
                        {'blockNumber': {'$lt': end}},
                    ]}
                }
        for i, r in enumerate(tqdm(transactions.aggregate([match]))):
            reqs = [UpdateOne({'_id': r['to']}, {'$addToSet': {'received_from': [r['from']]}}, upsert=True),
                UpdateOne({'_id': r['from']}, {'$addToSet': {'sent_to': [r['to']]}}, upsert=True),
                UpdateOne({'_id': r['to']}, {'$addToSet': {'any': [r['to'], r['from']]}}, upsert=True),
                UpdateOne({'_id': r['from']}, {'$addToSet': {'any': [r['to'], r['from']]}}, upsert=True)]

            counterparties.bulk_write(reqs, ordered=False) 

# sparse degree matrix
def write_counterparties_counts(counterparties):
    for counts in counterparties.aggregate([{'$project':
        {'numberFrom' : {'$size':  { '$ifNull': ['$received_from', []] } },
         'numberTo' : {'$size': { '$ifNull': ['$sent_to', []] } },
            'numberAny' : {'$size': { '$ifNull': ['$any', []] } }}
            }]):
        _id = counts['_id']
        del counts['_id']
        counterparties.update_one({'_id': _id}, {'$set': counts}, upsert=True)

def write_props(chain):
    client = MongoClient('localhost', 27017)
    db = client[chain]
    transactions = db['transactions']
    PREV = 2500000
    LATEST = 5000000
    STEP = 100000
    for blockNum in tqdm(range(PREV, LATEST, STEP)):
        cur_db = client[f'{chain}_historical_{blockNum}']
        if PREV >= STEP:
            if 'counterparties' not in cur_db.collection_names():
                client['admin'].command({"renameCollection": f'{chain}_historical_{blockNum-STEP}.counterparties', 'to': f'{chain}_historical_{blockNum}.counterparties'})
        #    if 'pairwise_values' not in cur_db.collection_names():
        #        client['admin'].command({"renameCollection": f'{chain}_historical_{blockNum-STEP}.pairwise_values', 'to': f'{chain}_historical_{blockNum}.pairwise_values'})
        #write_counterparties(transactions, cur_db['counterparties'], blockNum, blockNum+STEP) 
        write_counterparties_split(transactions, cur_db['counterparties'], blockNum, blockNum+STEP) 
        write_counterparties_counts(cur_db['counterparties']) 
        #write_pairwise_values(transactions, cur_db['pairwise_values'], blockNum, blockNum+STEP) 
        
write_props('eth')
