from ethanalysis.utils.normalization import normalize_address, convert_types
def get_block(w3, i):
    return convert_types(w3, w3.eth.getBlock(i))

def get_transaction(w3, tx_hash):
    transaction = w3.eth.getTransaction(tx_hash) 
    return f'{transaction.blockNumber}_{transaction.transactionIndex}', convert_types(w3, transaction) 

# returns description and reciept for a tx_hash
def get_receipt(w3, tx_hash):
    receipt = w3.eth.getTransactionReceipt(tx_hash) 
    return f'{receipt.blockNumber}_{receipt.transactionIndex}', convert_types(w3, receipt)

# ------------ probably used by mongo utils ----------

# generator for genesis accounts
def yield_genesis_accounts(w3, batch_size=500):
    prev = [None]
    while len(prev) > 0:
        result = None
        while result is None:
            try:
                result = w3.parity.listAccounts(batch_size, prev[-1], '0x0')
                result = map(normalize_address, result)
                if len(result) > 0:
                    yield from result
                else:
                    return
            except Exception as e:
                print(e)
        prev = result
