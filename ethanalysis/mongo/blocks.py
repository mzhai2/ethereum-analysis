def add_blocks_process(w3, earliest_block, latest_block, chain):
    from web3 import Web3, WebsocketProvider
    from pymongo import MongoClient, UpdateOne
    from ethanalysis.utils.w3 import get_block
    # w3 = Web3(WebsocketProvider('ws://suzhouchem.synology.me:8547'))
    client = MongoClient('localhost', 27017)
    db = client[chain]
    blocks = db['blocks']
    for i in range(earliest_block, latest_block):
        block = get_block(w3, i)
        reqs = [UpdateOne({'_id': block['number']}, {'$set': block}, upsert=True)]
        try:
            blocks.bulk_write(reqs, ordered=True)
        except pymongo.errors.BulkWriteError as bwe:
            print(bwe.details)
            blocks.bulk_write(reqs, ordered=True)

# adds the uncle miner to each block
def add_uncles_process(w3, earliest_block, latest_block, reset=False, chain='eth'):
    from pymongo import MongoClient, UpdateOne
    from pymongo.errors import BulkWriteError
    client = MongoClient('localhost', 27017)
    db = client[chain]
    blocks = db['blocks']
    if reset:
        blocks.update({}, {'$unset': {'uncleMiner': ''}}, multi=True)
    for block_missing_field in blocks.find({'$and':
        [{'_id': {'$gte': earliest_block}}, {'_id':{'$lt': latest_block}}, {'uncleMiner':{'$exists':False}}]
        }, no_cursor_timeout=True):
        i = block_missing_field['_id']
        j = 0
        reqs = []
        while True:
            uncle = w3.eth.getUncleByBlock(i, j)
            if uncle == None:
                if len(reqs) > 0:
                    try:
                        blocks.bulk_write(reqs, ordered=True)
                    except BulkWriteError as bwe:
                        print('retrying bulk write of uncle blocks')
                        blocks.bulk_write(reqs, ordered=True)
                break
            reqs.append(UpdateOne({'_id': i},
                                  {'$push':
                                      {'uncleMiners': uncle['miner']}
                                  },
                                  upsert=False))
            j+=1
