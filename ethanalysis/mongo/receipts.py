# inserts regular transactions from blocks db
def add_regular_receipts_process(earliest_block, latest_block):
    from pymongo import MongoClient
    from pymongo.errors import BulkWriteError
    from decimal import Decimal
    from bson.decimal128 import Decimal128
    from ethereum-analysis import get_receipt
    def init():
        client = MongoClient('localhost', 27017)
        db = client[chain]
        global p_transactions
        p_transactions = db['transactions']
        global p_blocks
        p_blocks = db['blocks']
        global p_receipts
        p_receipts = db['receipts']

    if 'p_transactions' not in globals():
        init()
    receipts = []
    for transaction in p_transactions.find({}, no_cursor_timeout=True):
        receipts.append(get_receipt(w3, transaction['hash']))
    reqs = [UpdateOne({'_id': receipt['transactionHash'], {'$set': receipt}, upsert=True) for receipt in receipts]
    try:
        receipts.bulk_write(reqs, ordered=True)
    except pymongo.errors.BulkWriteError as bwe:
        receipts.bulk_write(reqs, ordered=True)
        print(bwe.details)
