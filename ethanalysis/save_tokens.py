import time
import sys
import pymongo
from pymongo import MongoClient, IndexModel, UpdateOne, TEXT, ASCENDING
from tqdm import tqdm
from web3.utils.datastructures import AttributeDict
from web3 import Web3
from decimal import Decimal
from bson.decimal128 import Decimal128
import math
import functools
import itertools
from utils.normalization import normalize_address, convert_types
from utils.parallelization import TaskQueue, map_iter
import signal
import datetime

chain = sys.argv[1] 
PREV = int(sys.argv[2])
LATEST = int(sys.argv[3])

provider_ipc = Web3.IPCProvider(f'/home/mike/.local/share/io.parity.ethereum/jsonrpc-{chain}-a.ipc', timeout=40000)
w3 = Web3(provider_ipc)
client = MongoClient('localhost', 27017)
db = client[chain]
tokens = db['erc20']
abi = [ { "constant": True, "inputs": [], "name": "name", "outputs": [ { "name": "", "type": "string" } ], "payable": False, "stateMutability": "view", "type": "function" }, { "constant": False, "inputs": [ { "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "approve", "outputs": [ { "name": "", "type": "bool" } ], "payable": False, "stateMutability": "nonpayable", "type": "function" }, { "constant": True, "inputs": [], "name": "totalSupply", "outputs": [ { "name": "", "type": "uint256" } ], "payable": False, "stateMutability": "view", "type": "function" }, { "constant": False, "inputs": [ { "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "transferFrom", "outputs": [ { "name": "", "type": "bool" } ], "payable": False, "stateMutability": "nonpayable", "type": "function" }, { "constant": True, "inputs": [], "name": "decimals", "outputs": [ { "name": "", "type": "uint8" } ], "payable": False, "stateMutability": "view", "type": "function" }, { "constant": True, "inputs": [ { "name": "_owner", "type": "address" } ], "name": "balanceOf", "outputs": [ { "name": "balance", "type": "uint256" } ], "payable": False, "stateMutability": "view", "type": "function" }, { "constant": True, "inputs": [], "name": "symbol", "outputs": [ { "name": "", "type": "string" } ], "payable": False, "stateMutability": "view", "type": "function" }, { "constant": False, "inputs": [ { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" } ], "name": "transfer", "outputs": [ { "name": "", "type": "bool" } ], "payable": False, "stateMutability": "nonpayable", "type": "function" }, { "constant": True, "inputs": [ { "name": "_owner", "type": "address" }, { "name": "_spender", "type": "address" } ], "name": "allowance", "outputs": [ { "name": "", "type": "uint256" } ], "payable": False, "stateMutability": "view", "type": "function" }, { "payable": True, "stateMutability": "payable", "type": "fallback" }, { "anonymous": False, "inputs": [ { "indexed": True, "name": "owner", "type": "address" }, { "indexed": True, "name": "spender", "type": "address" }, { "indexed": False, "name": "value", "type": "uint256" } ], "name": "Approval", "type": "event" }, { "anonymous": False, "inputs": [ { "indexed": True, "name": "from", "type": "address" }, { "indexed": True, "name": "to", "type": "address" }, { "indexed": False, "name": "value", "type": "uint256" } ], "name": "Transfer", "type": "event" } ]


def delete_all():
    tokens.delete_many({})

def generate_indices(transactions, accounts):
    pass
    #transactions.create_indexes([IndexModel('from'), IndexModel('to'), IndexModel('blockNumber'), IndexModel('create')])
    #accounts.create_index('firstBlock', background=True)

def write_tokens(earliest_block, latest_block):
    def init():
        client = MongoClient('localhost', 27017)
        db = client[chain]
        global p_tokens
        p_tokens = db['erc20']

    if 'p_tokens' not in globals():
        init()

    event_filter = w3.eth.filter({'fromBlock': earliest_block, 'toBlock': latest_block, 'topics': ['0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef']})
    tokens = {(e['address'], e['blockNumber']) for e in event_filter.get_all_entries()}
    w3.eth.uninstallFilter(event_filter.filter_id)
    reqs = [UpdateOne({'_id': e[0]}, {'$min': {'firstTransfer': e[1]}}, upsert=True) for e in tokens]
    if len(reqs) == 0:
        return
    try:
        p_tokens.bulk_write(reqs, ordered=False)
    except pymongo.errors.BulkWriteError as bwe:
        print(bwe.details)
        p_tokens.bulk_write(reqs, ordered=False)

def write_transfers(earliest_block, latest_block):
    def init():
        client = MongoClient('localhost', 27017)
        global p_db
        p_db = client[chain]
        global p_tokens
        p_tokens = p_db['erc20']
        global p_token_db
        p_token_db = client[f'{chain}_tokens']

    if 'p_tokens' not in globals():
        init()

    for doc in p_tokens.find({}, no_cursor_timeout=True):
        contract = w3.eth.contract(address=doc['checksum'], abi=abi)
        event_filter = contract.events.Transfer.createFilter(fromBlock=earliest_block, toBlock=latest_block)
        reqs = []
        try:
            for log in event_filter.get_all_entries():
                tmp = convert_types(w3, log)
                tmp.update(tmp.pop('args'))
                blockNumber = tmp['blockNumber']
                logIndex = tmp['logIndex']
                reqs.append(UpdateOne({'_id': f'{blockNumber}_{logIndex}'}, {'$set': tmp}, upsert=True))
            w3.eth.uninstallFilter(event_filter.filter_id)
        except Exception as e:
            w3.eth.uninstallFilter(event_filter.filter_id)
            print(e, doc['_id'])
        if len(reqs) > 0:
            try:
                token_col = p_token_db[doc['checksum']]
                token_col.bulk_write(reqs, ordered=False)
            except pymongo.errors.BulkWriteError as bwe:
                print(bwe.details)
                token_col.bulk_write(reqs, ordered=False)

# derive a field using a task queue
def add_field_tq(collection, source, name, f, skips={}, to_hashable=lambda x: x):
    def process_field(init, res, f, source):
        if 'p_collections' not in globals():
            init()
        try:
            p_collection.update_one({'_id': res['_id']}, {
                    '$set': {name: f(res[source])}
                    }, upsert=False)
        except Exception as e :
            print(e)
            raise
    def worker_init():
        import pymongo
        client = MongoClient('localhost', 27017, maxPoolSize=1)
        db = client[chain]
        global p_collection
        p_collection = db[collection]
    q = TaskQueue(maxsize=-1, num_workers=n_workers, worker_init=worker_init)
    missing_field = db[collection].find({name: {'$exists': False}}, no_cursor_timeout=True)
    for res in tqdm(missing_field, total=missing_field.count(True)):
        if source not in res:
            continue
        if len(skips) > 0 and to_hashable(res[source]) in skips:
            continue
        q.add_task(process_field, res, f, source)
    qsize = q.qsize()
    while qsize > 0:
        time.sleep(30)
        qsize = q.qsize()
        print(qsize)
    q.join()
    q.exit()

def get_genesis_accounts(batch_size=500):
    prev = [None]
    while len(prev) > 0:
        result = None
        while result is None:
            try:
                result = w3.parity.listAccounts(batch_size, prev[-1], '0x0')
                if len(result) > 0:
                    yield from result
                else:
                    return
            except Exception as e:
                print(e)
        prev = result

def add_genesis_accounts(accounts, transactions):
    accounts.update_one({'_id': 'GENESIS'}, {'$set': {'firstBlock': 0}}, upsert=True)
    for i, addr in enumerate(tqdm(get_genesis_accounts())):
        checksum = normalize_address(addr)
        acct_reqs = [UpdateOne({'_id': addr}, {'$set': {'checksum': checksum, 'firstBlock': 0}}, upsert=True)]
        tx_reqs = [UpdateOne({'_id': str(i)}, {'$set': {'blockHash': '0xd4e56740f876aef8c010b86a40d5f56745a118d0906a34e69aec8c0db1cb8fa3', 'blockNumber': 0, 'timestamp': 0, 'from': 'GENESIS', 'value': Decimal128(Decimal(w3.eth.getBalance(checksum, 0))), 'to': addr}}, upsert=True)]
        try:
            accounts.bulk_write(acct_reqs, ordered=False)
            transactions.bulk_write(tx_reqs, ordered=False)
        except pymongo.errors.BulkWriteError as bwe:
            print(bwe.details)
            raise
        
# process each account in a task queue
def update_account_wealth(earliest_block=None, reset=False):
    def _update_account_wealth(init, account, earliest_block):
        if 'p_transactions' not in globals():
            init()
    
        if earliest_block:
            match = {'$match': {'$and':
                [{'$or': [{'from': account}, {'to': account}, {'creates': account}] },
                {'blockNumber': {'$gte': earliest_block}}]
                }}
        else:
            match = {'$match': {'$or': [{'from': account}, {'to': account}, {'creates': account}] }}
        proj = {'$project':
                   {'from':1,
                    'to': {'$ifNull': ['$creates', '$to']},
                    'value': {'$cond': [{'$eq': ['$from', account]}, {'$multiply': ['$value', -1]}, '$value']},
                    'blockNumber': 1
                   }
               }
        try:
            for tx in p_transactions.aggregate([match, proj]):
                value = tx['value'].to_decimal()
                update_account = p_accounts.find_one({'_id': account})
    
                if 'currentBalance' in update_account:
                    currentBalance = update_account['currentBalance'].to_decimal()
                else:
                    currentBalance = 0
    
                if value > 0:
                    p_accounts.update_one({'_id': tx['to']}, {'$max': {'lastBlock': tx['blockNumber'], 'maxBalance': Decimal128(currentBalance+value)}, '$inc': {'currentBalance': tx['value']}}, upsert=False)
                else:
                    p_accounts.update_one({'_id': tx['from']}, {'$max': {'lastBlock': tx['blockNumber']},'$inc': {'currentBalance': tx['value']}}, upsert=False)
        except Exception as e:
            print(e)
            if 'tx' in locals():
                print(tx)
            raise

    def worker_init():
        def exit_handler(signum, frame):
            p_client.close()
        global p_client
        p_client = MongoClient('localhost', 27017)
        db = p_client[chain]
        global p_accounts
        p_accounts = db['accounts']
        global p_transactions
        p_transactions = db['transactions']
        signal.signal(signal.SIGINT, exit_handler)
        signal.signal(signal.SIGTERM, exit_handler)

    if reset:
        accounts.update({}, {'$unset': {'currentBalance': '', 'maxBalance': ''}}, multi=True)

    accounts_cursor = accounts.find({}, no_cursor_timeout=True, batch_size=1)
    q = TaskQueue(maxsize=-1, num_workers=n_workers, worker_init=worker_init)
    for a in tqdm(accounts_cursor, total=accounts_cursor.count(True)):
        q.add_task(_update_account_wealth, a['_id'], earliest_block)
    qsize = q.qsize()
    while qsize > 0:
        time.sleep(30)
        qsize = q.qsize()
        print(qsize)
    q.join()
    q.exit()
    del accounts_cursor

n_workers = 10
chunksize = 1

step = 1000
total = math.ceil((LATEST-PREV)/step)
yield_chunk = False 
if yield_chunk:
    total /= chunksize
earliest = range(PREV, LATEST, step)
latest = range(PREV+step, LATEST+step, step)
# delete_all(transactions, blocks, accounts, receipts)
# 
# for _ in tqdm(map_iter(
#     write_tokens,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass

step = 10000
total = math.ceil((LATEST-PREV)/step)
yield_chunk = False 
if yield_chunk:
    total /= chunksize
earliest = range(PREV, LATEST, step)
latest = range(PREV+step, LATEST+step, step)

# add_field_tq('erc20', '_id', 'checksum', normalize_address, {'GENESIS', 'COINBASE'})
for _ in tqdm(map_iter(
    write_transfers,
    n_workers,
    thread=False,
    yield_chunk=yield_chunk,
    chunksize=chunksize
    )(earliest, latest), total=total):
    pass
