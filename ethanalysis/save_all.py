import time
import sys
import pymongo
from pymongo import MongoClient, UpdateOne, TEXT, ASCENDING
from tqdm import tqdm
from web3 import Web3
from decimal import Decimal
from bson.decimal128 import Decimal128
import math
import functools
import itertools
from utils.normalization import normalize_address, convert_types
from utils.parallelization import TaskQueue, map_iter
from utils.mongo import add_field_tq, delete_all, generate_indices
import datetime

step = 100
chain = sys.argv[1] 
PREV = int(sys.argv[2])
LATEST = int(sys.argv[3])


provider_ipc = Web3.IPCProvider(f'/home/mike/.local/share/io.parity.ethereum/jsonrpc-{chain}-a.ipc', timeout=400)
w3 = Web3(provider_ipc)
client = MongoClient('localhost', 27017)
db = client[chain]
transactions = db['transactions']
blocks = db['blocks']
accounts = db['accounts']
receipts = db['receipts']

n_workers = 40
chunksize = 1
total = math.ceil((LATEST-PREV)/step)
yield_chunk = False 
if yield_chunk:
    total /= chunksize
earliest = range(PREV, LATEST, step)
latest = range(PREV+step, LATEST+step, step)

# delete_all(transactions, blocks, accounts, receipts)

# for _ in tqdm(map_iter(
#     add_blocks_process,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass

# for _ in tqdm(map_iter(
#     add_regular_transactions_process,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass

# for _ in tqdm(map_iter(
#     add_accounts_process,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass
# 
# for _ in tqdm(map_iter(
#     add_receipts_process,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass

# generate_indices(transactions, accounts)
# add_genesis_accounts(accounts, transactions)
# 
# for _ in tqdm(map_iter(
#     add_uncles_process,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass
# 
# for _ in tqdm(map_iter(
#     add_mining_transactions_process,
#     n_workers,
#     thread=False,
#     yield_chunk=yield_chunk,
#     chunksize=chunksize
#     )(earliest, latest), total=total):
#     pass
# 
# 
# add_field_tq('accounts', '_id', 'checksum', normalize_address, {'GENESIS', 'COINBASE'})
# add_field_tq('accounts', 'checksum', 'bytecode', w3.eth.getCode)
# def len_gt_zero(s):
#     if len(s) > 0:
#         return True
#     return False
# add_field_tq('accounts', 'bytecode', 'eoa', len_gt_zero)
blocks.update_one({'_id': 0}, {'$set': {'timestamp': blocks.find_one({'_id': 1})['timestamp']}})
def s_to_date(s):
    return datetime.datetime.fromtimestamp(s.to_decimal())
add_field_tq('blocks', 'timestamp', 'date', s_to_date, to_hashable=lambda x: x.to_decimal())

# update_account_wealth(PREV, False)

# i = 0
# for j,v in enumerate(accounts.find({'checksum': {'$exists': False}})):
#     print(v)
#     i=j
# print(i)
# assert i == 0
