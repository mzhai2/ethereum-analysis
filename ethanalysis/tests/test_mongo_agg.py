from pymongo import MongoClient
db = MongoClient().aggregation_example
db.things.delete_many({})
db.things.insert_many([{ '_id' : 1 , 'data' : 'f', 'data2' : 'a' },
{ '_id' : 2 , 'data' : '0', 'data2' : 'a' },
{ '_id' : 3 , 'data' : '1', 'data2' : 'b' }])


res = db.things.aggregate([
    {
        '$group': {
            '_id': 0,
            'data': { '$addToSet':'$data'},
            'data2': {'$addToSet':'$data2'}
            }
    },
    {
       "$project": {
           'combined': {'$setUnion': ['$data', '$data2']},
           }
    },
])

for r in res:
    print(r)

