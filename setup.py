import setuptools
with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='ethanalysis',  
    version='0.1',
    #scripts=[''],
    author="Mutian Zhai",
    author_email="mutian.zhai@gmail.com",
    description="Ethereum data analysis",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mzhai2/ethereum-analysis",
    packages=setuptools.find_packages(),
    classifiers=[
    ],
 )
