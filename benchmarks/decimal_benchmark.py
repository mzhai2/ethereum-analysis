from decimal import Decimal
from bson.decimal128 import Decimal128
from time import time

def i_benchmark(iters=100000, n = 123456789):
    start = time()
    for _ in range(iters):
        a = n
    print(time()-start)

def d_benchmark(iters=100000, n = 123456789):
    start = time()
    for _ in range(iters):
        Decimal(n)
    print(time()-start)

def d128_benchmark_str(iters=100000, n = 123456789):
    start = time()
    for _ in range(iters):
        Decimal128(str(n))
    print(time()-start)

def d128_benchmark_decimal(iters=100000, n = 123456789):
    start = time()
    for _ in range(iters):
        Decimal128(Decimal(n))
    print(time()-start)

i_benchmark()
d_benchmark()
