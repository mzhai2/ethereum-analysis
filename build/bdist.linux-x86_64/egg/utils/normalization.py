from web3 import Web3
from web3.utils.datastructures import AttributeDict
import decimal
from decimal import Decimal
from bson.decimal128 import Decimal128, create_decimal128_context

# normalization a block given by w3
def inplace_normalize_block(block):
    for i in range(len(block['transactions'])):
        if block['transactions'][i]['to']:
            if block['transactions'][i]['to'] == '0x0':
                block['transactions'][i]['to'] = '0x0000000000000000000000000000000000000000'
            else:
                block['transactions'][i]['to'] = Web3.toChecksumAddress(block['transactions'][i]['to'])
        block['transactions'][i]['from'] = Web3.toChecksumAddress(block['transactions'][i]['from'])

def normalize_address(address):
    if address == '0x0':
        return '0x0000000000000000000000000000000000000000'
    else:
        return Web3.toChecksumAddress(address)

def convert_types(w3, obj, skips={'blockNumber', 'transactionIndex', 'logIndex'}):
    if isinstance(obj, bytes):
        return w3.toHex(obj)
    elif isinstance(obj, int):
            #try:
            with decimal.localcontext(create_decimal128_context()) as ctx:
                ctx.traps[decimal.Inexact] = 0
                d = Decimal128(ctx.create_decimal(obj))
            #except Exception as e:
            #    print(obj, e)
            #    raise
            return d
    elif isinstance(obj, list):
        return [convert_types(w3, v) for v in obj]
    elif isinstance(obj, AttributeDict):
        newDict = {}
        for i in obj.items():
            if i[0] in skips:
                newDict[i[0]] = i[1]
            else:
                newDict[i[0]] = convert_types(w3, i[1])
        return newDict
    else:
        return obj

