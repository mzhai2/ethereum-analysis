def add_blocks_process(earliest_block, latest_block):
    from pymongo import MongoClient
    from ethereum-analysis.utils.w3 import get_block
    client = MongoClient('localhost', 27017)
    db = client[chain]
    blocks = db['blocks']
    _blocks = [get_block(w3, i) for i in range(earliest_block, latest_block)]
    reqs = [UpdateOne({'_id': b_id}, {'$set': b}, upsert=True) for b_id, b in _blocks]
    try:
        blocks.bulk_write(reqs, ordered=True)
    except pymongo.errors.BulkWriteError as bwe:
        print(bwe.details)
        blocks.bulk_write(reqs, ordered=True)

# adds the uncle miner to each block
def add_uncles_process(w3, earliest_block, latest_block, reset=False):
    from pymongo import MongoClient, UpdateOne
    from pymongo.errors import BulkWriteError
    client = MongoClient('localhost', 27017)
    db = client[chain]
    blocks = db['blocks']
    if reset:
        blocks.update({}, {'$unset': {'uncleMiner': ''}}, multi=True)
    for block_missing_field in blocks.find({'$and':
        [{'_id': {'$gte': earliest_block}}, {'_id':{'$lt': latest_block}}, {'uncleMiner':{'$exists':False}}]
        }, no_cursor_timeout=True):
        i = block_missing_field['_id']
        j = 0
        reqs = []
        while True:
            uncle = w3.eth.getUncleByBlock(i, j)
            if uncle == None:
                if len(reqs) > 0:
                    try:
                        blocks.bulk_write(reqs, ordered=True)
                    except BulkWriteError as bwe:
                        print('retrying bulk write of uncle blocks')
                        blocks.bulk_write(reqs, ordered=True)
                break
            reqs.append(UpdateOne({'_id': i},
                                  {'$push':
                                      {'uncleMiners': uncle['miner']}
                                  },
                                  upsert=False))
            j+=1
