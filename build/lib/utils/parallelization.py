from multiprocess.dummy import Pool as ThreadPool
from multiprocess import Pool, Process, get_context
from multiprocess.queues import JoinableQueue as Queue
from functools import wraps
import concurrent.futures

class TaskQueue(Queue):

    def __init__(self, maxsize=-1, block=True, timeout=None, num_workers=1, worker_init=None):
        super().__init__(maxsize, ctx=get_context())
        self.worker_init = worker_init
        self.num_workers = num_workers
        self.block = block
        self.timeout = timeout
        self.workers = []
        self.start_workers()

    def add_task(self, task, *args, **kwargs):
        args = args or ()
        kwargs = kwargs or {}
        self.put((task, args, kwargs), block=self.block, timeout=self.timeout)

    def start_workers(self):
        for i in range(self.num_workers):
            t = Process(target=self.worker)
            self.workers.append(t)
            t.start()

    def worker(self):
        while True:
            item, args, kwargs = self.get()
            if self.worker_init is not None:
                item(self.worker_init, *args, **kwargs)  
            else:
                item(*args, **kwargs)  
            self.task_done()

    def exit(self):
        for w in self.workers:
            w.terminate()

def variable_grouper(iterable, n):
    "Collect data into variable length chunks or blocks, removing Nones"
    # grouper('ABCDEFG', 3) --> ABC DEF G"
    args = [iter(iterable)] * n
    return [[arg for arg in i if arg is not None] for i in zip_longest(*args, fillvalue=None)]

# if your function has named arguments, wrap with a lambda and convert to positional
# multithread_map(add)([1,1,1],[1,2,3], N=64)
# multithread_map(lambda x,y: requests.post(x, json=y))(['http://api.eosnewyork.io:/v1/chain/get_block']*10, [{"block_num_or_id": num} for num in range(start_block+chunk*i, start_block+chunk*(i+1))], N=32)
def star(f):
    @wraps(f)
    def wrapper(args):
        return f(*args)
    return wrapper

def map_iter(f, n_workers, thread=True, yield_chunk=False, ordered=True, chunksize=1):
    @wraps(f)
    def wrapper(*args):
        if thread:
            pool = ThreadPool(n_workers)
        else:
            pool = Pool(n_workers)
        # args are iterables
        # ith iterable should return the ith argument
        args = zip(*args)

        if yield_chunk:
            yield pool.starmap_async(f, args, chunksize).get()
        else:
            if ordered:
                for res in pool.imap(star(f), args, chunksize):
                    yield res
            else:
                for res in pool.imap_unordered(star(f), args, chunksize):
                    yield res
        pool.close()
        pool.join()
    return wrapper

def thread_executor_map_iter(f, n_threads):
    @wraps(f)
    def wrapper(*args):
        with concurrent.futures.ThreadPoolExecutor(max_workers=n_threads) as executor:
            args = zip(*args)
            futures = []
            for a in args:
                futures.append(executor.submit(f,*a))
            for future in concurrent.futures.as_completed(futures):
                yield future.result()
    return wrapper

