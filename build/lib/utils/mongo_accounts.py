# inserts the genesis accounts 
def add_genesis_accounts(w3, accounts):
    from pymongo import UpdateOne
    from pymongo.errors import BulkWriteError
    from bson.decimal128 import Decimal128
    from ethereum-analysis.utils.w3 import yield_genesis_accounts
    from ethereum-analysis.utils.normalization import normalize_address 

    accounts.update_one({'_id': 'GENESIS'}, {'$set': {'firstBlock': 0}}, upsert=True)
    for i, addr in enumerate(tqdm(yield_genesis_accounts(w3))):
        checksum = normalize_address(addr)
        acct_reqs = [UpdateOne({'_id': addr}, {'$set': {'checksum': checksum, 'firstBlock': 0}}, upsert=True)]
        try:
            accounts.bulk_write(acct_reqs, ordered=False)
        except BulkWriteError as bwe:
            print(bwe.details)
            print('retrying bulk write of genesis accounts')
            accounts.bulk_write(acct_reqs, ordered=False)

def add_accounts_process(earliest_block, latest_block):
    def init():
        client = MongoClient('localhost', 27017)
        db = client[chain]
        global p_transactions
        p_transactions = db['transactions']
        global p_blocks
        p_blocks = db['blocks']
        global p_accounts
        p_accounts = db['accounts']

    if 'p_transactions' not in globals():
        init()

    tx_cursor = p_transactions.find({'$and':[{'blockNumber': {'$gte': earliest_block}}, {'blockNumber':{'$lt': latest_block}}]}, no_cursor_timeout=True)
    for tx in tx_cursor:
        to = tx['to'] or tx['creates']
        assert to is not None, tx
        assert tx['from'] is not None, tx
        reqs = [UpdateOne({'_id': tx['from']}, {'$min': {'firstBlock': tx['blockNumber']}}, upsert=True),
                UpdateOne({'_id': to}, {'$min': {'firstBlock': tx['blockNumber']}}, upsert=True)]
        try:
            p_accounts.bulk_write(reqs, ordered=False)
        except pymongo.errors.BulkWriteError as bwe:
            print(bwe.details)
            p_accounts.bulk_write(reqs, ordered=False)
    del tx_cursor 
    block_cursor = p_blocks.find({'$and':[{'_id': {'$gte': earliest_block}}, {'_id':{'$lt': latest_block}}]}, no_cursor_timeout=True)
    for block in block_cursor:
        assert type(block['_id']) is int
        reqs = [UpdateOne({'_id': block['miner']}, {'$min': {'firstBlock': block['_id']}}, upsert=True)]
        if 'uncle_miners' in block:
            for uncle_miner in block['uncle_miners']:
                assert uncle_miner is not None, block
                reqs.append(UpdateOne({'_id': uncle_miner}, {'$min': {'firstBlock': block['_id']}}, upsert=True))
        try:
            p_accounts.bulk_write(reqs, ordered=False)
        except pymongo.errors.BulkWriteError as bwe:
            print(bwe.details)
            p_accounts.bulk_write(reqs, ordered=False)
    del block_cursor 

# process each account in a task queue
# calculates the current and maximum balance in ETH on each account
# we use earliest block when updating the account DB
def update_account_wealth(earliest, latest, reset=False):
    from pymongo import MongoClient, UpdateOne
    from pymongo.errors import BulkWriteError
    from bson.decimal128 import Decimal128
    from utils.parallelization import TaskQueue
    import signal
    def _update_account_wealth(init, account, earliest_block):
        if 'p_transactions' not in globals():
            init()
    
        # only select blocks after earliest_block
        match = {'$match': {'$and':
            [{'$or': [{'from': account}, {'to': account}, {'creates': account}] },
            {'blockNumber': {'$gte': earliest}},
            {'blockNumber': {'$lt': latest}}]
            }}
        # if match is sender multiply value by -1
        proj = {'$project':
                   {'from':1,
                    'to': {'$ifNull': ['$creates', '$to']},
                    'value': {'$cond': [{'$eq': ['$from', account]}, {'$multiply': ['$value', -1]}, '$value']},
                    'blockNumber': 1
                   }
               }
        try:
            # for each transaction
            reqs = []
            for tx in p_transactions.aggregate([match, proj]):
                value = tx['value'].to_decimal()
                update_account = p_accounts.find_one({'_id': account})
    
                if 'currentBalance' in update_account:
                    currentBalance = update_account['currentBalance'].to_decimal()
                else:
                    currentBalance = 0

                # TODO lastBlock not set for other party... or add fields for last block sent/recv
                # account is recipient
                if value > 0:
                    # lastBlock - most recently active block, $max sets field if dne
                    reqs.append(UpdateOne({'_id': tx['to']}, {'$max': {'lastBlock': tx['blockNumber'], 'maxBalance': Decimal128(currentBalance+value)}, '$inc': {'currentBalance': tx['value']}}, upsert=False))
                # account is sender
                else:
                    reqs.append(UpdateOne({'_id': tx['from']}, {'$max': {'lastBlock': tx['blockNumber']},'$inc': {'currentBalance': tx['value']}}, upsert=False))
	    try:
		p_accounts.bulk_write(reqs, ordered=False)
	    except BulkWriteError as bwe:
		print('retrying bulk write of block/uncle rewards and fees')
		p_accounts.bulk_write(reqs, ordered=False)
        except Exception as e:
            print(e)
            raise

    def worker_init():
        def exit_handler(signum, frame):
            p_client.close()
        global p_client
        p_client = MongoClient('localhost', 27017)
        db = p_client[chain]
        global p_accounts
        p_accounts = db['accounts']
        global p_transactions
        p_transactions = db['transactions']
        signal.signal(signal.SIGINT, exit_handler)
        signal.signal(signal.SIGTERM, exit_handler)

    if reset:
        accounts.update({}, {'$unset': {'lastBlock': '', 'currentBalance': '', 'maxBalance': ''}}, multi=True)

    accounts_cursor = accounts.find({}, no_cursor_timeout=True, batch_size=1)
    q = TaskQueue(maxsize=-1, num_workers=n_workers, worker_init=worker_init)
    for a in tqdm(accounts_cursor, total=accounts_cursor.count(True)):
        q.add_task(_update_account_wealth, a['_id'], earliest_block)
    qsize = q.qsize()
    while qsize > 0:
        time.sleep(30)
        qsize = q.qsize()
        print(qsize)
    q.join()
    q.exit()
    del accounts_cursor
