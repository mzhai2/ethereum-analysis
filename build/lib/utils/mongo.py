def delete_all(transactions, blocks, accounts, receipts):
    transactions.delete_many({})
    accounts.delete_many({})
    blocks.delete_many({})
    receipts.delete_many({})

def generate_indices(transactions, accounts):
    from pymongo import IndexModel
    transactions.create_indexes([IndexModel('from'), IndexModel('to'), IndexModel('blockNumber'), IndexModel('create')])
    accounts.create_index('firstBlock', background=True)

# derive a field using a task queue
# skip if to_hashable(the source field) is in the skip set
def add_field_tq(collection, source, name, f, skips={}, to_hashable=lambda x: x):
    from ethereum-analysis.utils.parallelization import TaskQueue
    def process_field(init, res, f, source):
        if 'p_collections' not in globals():
            init()
        try:
            p_collection.update_one({'_id': res['_id']}, {
                    '$set': {name: f(res[source])}
                    }, upsert=False)
        except Exception as e :
            print(e)
            raise
    def worker_init():
        from pymongo import MongoClient
        client = MongoClient('localhost', 27017)
        db = client[chain]
        global p_collection
        p_collection = db[collection]
    q = TaskQueue(maxsize=-1, num_workers=n_workers, worker_init=worker_init)
    missing_field = db[collection].find({name: {'$exists': False}}, no_cursor_timeout=True)
    for res in tqdm(missing_field, total=missing_field.count(True)):
        if source not in res:
            continue
        if len(skips) > 0 and to_hashable(res[source]) in skips:
            continue
        q.add_task(process_field, res, f, source)
    qsize = q.qsize()
    while qsize > 0:
        time.sleep(30)
        qsize = q.qsize()
        print(qsize)
    q.join()
    q.exit()



