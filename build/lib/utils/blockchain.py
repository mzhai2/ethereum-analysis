import numpy as np
from typing import Dict, Tuple, List, Set

def is_EOA(w3, address) -> bool:
    if len(w3.eth.getCode(address)) > 0:
        return False
    return True

def separate_accounts_by_type(accounts) -> Tuple[List[str], List[str]]:
    eoas = []
    contracts = []
    for account in accounts:
        if is_EOA(account):
            eoas.append(account)
        else:
            contracts.append(account)
    return eoas, contracts

def block_time(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')

# (U_n + 8 - B_n) * R / 8
def calc_uncle_reward(w3, current_blockNumber, uncle_block, reward, era):
    if chain == 'eth' or (chain == 'etc' and era == 0):
        t = w3.toInt(hexstr=uncle_block['number']) + 8 - current_blockNumber
        return t*reward/8
    elif chain == 'etc' and era > 0:
        return 1/32*reward
    raise NotImplementedError

def calc_tx_fees(w3, block):
    return sum([w3.eth.getTransaction(tx)['gasPrice']*w3.eth.getTransactionReceipt(tx)['gasUsed'] for tx in block['transactions']])

