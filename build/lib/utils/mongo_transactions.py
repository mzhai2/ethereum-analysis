# inserts regular transactions from blocks db
def add_regular_transactions_process(earliest_block, latest_block):
    from pymongo import MongoClient
    from pymongo.errors import BulkWriteError
    from decimal import Decimal
    from bson.decimal128 import Decimal128
    def init():
        client = MongoClient('localhost', 27017)
        db = client[chain]
        global p_transactions
        p_transactions = db['transactions']
        global p_blocks
        p_blocks = db['blocks']

    if 'p_transactions' not in globals():
        init()
    transactions = []
    for block in p_blocks.find({}, no_cursor_timeout=True):
        if len(block['transactions']) > 0:
            transactions.append(get_transaction(w3, tx_hash) for tx_hash in block['transactions'])
    if len(transactions) > 0:
        reqs = [UpdateOne({'_id': f'{tx.blockNumber}_{tx.transactionIndex}', {'$set': tx}, upsert=True) for tx in transactions]
        try:
            transactions.bulk_write(reqs, ordered=True)
        except pymongo.errors.BulkWriteError as bwe:
            transactions.bulk_write(reqs, ordered=True)
            print(bwe.details)
            
# inserts the genesis transactions 
def add_genesis_transactions(w3, transactions):
    from pymongo import UpdateOne
    from pymongo.errors import BulkWriteError
    from decimal import Decimal
    from bson.decimal128 import Decimal128
    from ethereum-analysis.utils.w3 import yield_genesis_accounts
    from ethereum-analysis.utils.normalization import normalize_address 

    for i, addr in enumerate(tqdm(yield_genesis_accounts(w3))):
        checksum = normalize_address(addr)
        tx_reqs = [UpdateOne({'_id': str(i)}, {'$set': {'blockHash': '0xd4e56740f876aef8c010b86a40d5f56745a118d0906a34e69aec8c0db1cb8fa3', 'blockNumber': 0, 'timestamp': 0, 'from': 'GENESIS', 'value': Decimal128(Decimal(w3.eth.getBalance(checksum, 0))), 'to': addr}}, upsert=True)]
        try:
            transactions.bulk_write(tx_reqs, ordered=False)
        except BulkWriteError as bwe:
            print(bwe.details)
            print('retrying bulk write of transactions')
            transactions.bulk_write(tx_reqs, ordered=False)

# id is miner_{blockNumber}
# and miner_{uncle_{blockNumber}_{uncleNumber}
# I believe that at one fork, these started being included as internal transactions
# probably best to ignore these for now
def add_mining_transactions_process(w3, earliest_block, latest_block):
    from pymongo import MongoClient
    from pymongo.errors import BulkWriteError
    from decimal import Decimal
    from bson.decimal128 import Decimal128
    def init():
        client = MongoClient('localhost', 27017)
        db = client[chain]
        global p_transactions
        p_transactions = db['transactions']
        global p_blocks
        p_blocks = db['blocks']

    if 'p_transactions' not in globals():
        init()
    reqs = []
    for block in p_blocks.find({'$and':
        [{'_id': {'$gte': earliest_block}}, {'_id':{'$lt': latest_block}}]
        }, no_cursor_timeout=True):
        reward = 5000000000000000000
        era = None
        if chain == 'eth':
            if block['_id'] >= 4370000:
                reward = 3000000000000000000 
        if chain == 'etc':
            era = block['_id'] // 5000000
            reward *= 0.8**era

        # TODO: consider adding transactions for tx fees from the sender to the miner, tagged with "FEE"
        reqs.append(UpdateOne({'_id': 'blockReward_{}'.format(block['_id'])}, {'$set':{'blockHash': block['hash'],
               'blockNumber': block['_id'], 'timestamp': block['timestamp'], 'from': 'COINBASE',
               'to': block['miner'], 'value': Decimal128(Decimal(reward+reward/32*len(block['uncles']))), tags: ['blockReward']}},
               upsert=True))
        reqs.append(UpdateOne({'_id': 'fee_{}'.format(block['_id'])}, {'$set':{'blockHash': block['hash'],
               'blockNumber': block['_id'], 'timestamp': block['timestamp'], 'from': 'COINBASE',
               'to': block['miner'], 'value': calc_tx_fees(block), 'tags': ['fee']))}},
               upsert=True))

        for u, uncle_hash in enumerate(block['uncles']):
            uncle_block = w3.eth.getUncleByBlock(block['_id'], u)
            assert uncle_block is not None
            try:
                reqs.append(UpdateOne({'_id': '{}_{}'.format('_'.join(tags), block['_id'])},
                {'$set':{'blockHash': block['hash'], 'blockNumber': block['_id'],
                'timestamp': block['timestamp'], 'from': 'COINBASE', 'to': uncle_block['miner'],
                'value': Decimal128(Decimal(calc_uncle_reward(block['_id'], uncle_block, reward, era))), 'tags': ['uncleReward', str(u)]}},
                upsert=True))
            except Exception:
                print(uncle_block)
                raise
    try:
        p_transactions.bulk_write(reqs, ordered=False)
    except BulkWriteError as bwe:
        print('retrying bulk write of block/uncle rewards and fees')
        p_transactions.bulk_write(reqs, ordered=False)
