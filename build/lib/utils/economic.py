from tqdm import tqdm
import utils

def basic_stats(balances, ether=False):
    if ether:
        _balances = [b/1e18 for b in balances]
    else:
        _balances = balances
    total_v = sum(_balances)
    mean_v = np.mean(_balances)
    median_v = np.median(_balances)
    max_v = max(_balances)
    min_v = min(_balances)
    return total_v, mean_v, median_v, max_v, min_v

def gini(array):
    """Calculate the Gini coefficient of a numpy array."""
    # based on bottom eq:
    # http://www.statsdirect.com/help/generatedimages/equations/equation154.svg
    # from:
    # http://www.statsdirect.com/help/default.htm#nonparametric_methods/gini.htm
    # All values are treated equally, arrays must be 1d:
    array = array.flatten()
    if np.amin(array) < 0:
        # Values cannot be negative:
        array -= np.amin(array)
    # Values cannot be 0:
    array += 0.0000001
    # Values must be sorted:
    array = np.sort(array)
    # Index per array element:
    index = np.arange(1,array.shape[0]+1)
    # Number of array elements:
    n = array.shape[0]
    # Gini coefficient:
    return ((np.sum((2 * index - n  - 1) * array)) / (n * np.sum(array)))

def calculate_balances(w3, block, accounts, sort=False):
    balances = {}
    match = {
                '$match': {'firstBlock': {'$lt':block}}
            }
    _accounts = list(map(lambda x: x['checksum'], accounts.aggregate([match])))
    res = list(utils.map_iter(w3.eth.getBalance, 16)(_accounts, [block]*len(_accounts)))
    balances = zip(_accounts, res)
    if sort:
        balances = sorted(balances, key=lambda x: x[1])
    return balances

# calculate balances using only stored transactions in mongo
#def calculate_balances(accounts, transactions, block):
#    account_bal = {}
#    match = {
#                '$match': {'blockNumber': {'$lt':end}}
#            }
#    # transactions
#    for i, tx in enumerate(transactions.aggregate([match])):
#        account_bal[tx['to']] += tx['value']
#        account_bal[tx['from']] -= tx['value']
#    del account_bal['GENESIS']

# calculate the number of unique counterparties within a block interval
def calculate_counterparties(accounts, transactions, final, step):
    cps = []
    cps_excluding_one = []
    start = step
    cache = {}
    for end in range(start, final, step): 
        # find the txs where the block number is between a and b
        match = {'$match':
                    {'$and':[
                        {'blockNumber': {'$gte':end-step}},
                        {'blockNumber': {'$lt':end}},
                    ]}
                }
        for i, r in enumerate(tqdm(transactions.aggregate([match]))):
            #if i % 100 == 0:
            #    print(list(cache.items()))
            tx_hash = r['_id']
            to_from = [r['to'], r['from']]
            for acct in to_from:
                if acct in cache:
                    cache[acct].update(to_from)
                    counterparties = cache[acct]
                else:
                    counterparties = set(to_from) 
                    cache[acct] = counterparties
                #if 'GENESIS' in counterparties:
                # includes self so -1
        total_accounts = len(cache)
        lengths = list(map(len, cache.values()))
        total_counterparties = sum(lengths)-total_accounts
        lengths_excl = list(filter(lambda x: x > 2, lengths))
        total_accounts_excl = len(lengths_excl)
        total_counterparties_excluding_one = sum(lengths_excl)-total_accounts_excl
        total_accounts_excluding_one = len(lengths_excl)

        cps.append(total_counterparties/total_accounts)
        print(f'from {end-step} to {end}')
        print(f'total cps {total_counterparties} total accounts {total_accounts} cp/acct {cps[-1]}')
        cps_excluding_one.append(total_counterparties_excluding_one/total_accounts_excluding_one)
        print(f'total cps ex1 {total_counterparties_excluding_one} total accounts ex1 {total_accounts_excluding_one} cp/accts {cps_excluding_one[-1]}\n')
    return cps, cps_excluding_one

# calculate the number of transactions per account
def calculate_transactions(accounts, transactions, final, step):
    txs = []
    txs_excluding_one = []
    start = step
    cache = {}
    for end in range(start, final, step): 
        # find the txs where the block number is between a and b
        match = {'$match':
                    {'$and':[
                        {'blockNumber': {'$gte':end-step}},
                        {'blockNumber': {'$lt':end}},
                    ]}
                }
        for i, r in enumerate(tqdm(transactions.aggregate([match]))):
            tx_hash = [r['_id']]
            acct = r['from']
            if acct in cache:
                cache[acct].update(tx_hash)
                counterparties = cache[acct]
            else:
                counterparties = set(tx_hash) 
                cache[acct] = counterparties
        total_accounts = len(cache)
        lengths = list(map(len, cache.values()))
        total_transactions = sum(lengths)
        lengths_excl = list(filter(lambda x: x > 2, lengths))
        total_accounts_excl = len(lengths_excl)
        total_transactions_excluding_one = sum(lengths_excl)-total_accounts_excl
        total_accounts_excluding_one = len(lengths_excl)

        txs.append(total_transactions/total_accounts)
        print(f'from {end-step} to {end}')
        print(f'total txs {total_transactions} total accounts {total_accounts} cps/accts {txs[-1]}')
        txs_excluding_one.append(total_transactions_excluding_one/total_accounts_excluding_one)
        print(f'total txs ex1 {total_transactions_excluding_one} total accounts ex1 {total_accounts_excluding_one} txs/accts {txs_excluding_one[-1]}\n')
    return txs, txs_excluding_one

