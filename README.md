## Parity

### run with remote access on port 8547:
`bin/parity -c .local/share/io.parity.ethereum/eth-a-remote.toml`


### backup
`tar c -C ~/fast parity | plzip -1c | pv > /mnt/backup/parity.tar.lzip`
lzip0 compression saved 80GB on 1.6 TB orig because RocksDB already compressed... not worth the complexity
### restore
`plzip -dc /mnt/backup/parity.tar.lzip | tar x -C ~/fast`

### Schemas

#### Blocks
|column name    |type           |possible values |
|:-------------:|:-------------:|:-----:|
| _id           | block number  | 1+
  blockNumber:
  miner
  uncleMiner
#### Transactions
|column name     | type         | possible values |
|:-------------:|:-------------:|:-----:|
| _id            | '_'.join(tags)_blockNumber | tags_2 |
| tags | [str]   | [external, internal, blockReward, uncleReward, fee] |

## Notes
[if bulk write fails just try again] (https://stackoverflow.com/questions/29305405/mongodb-impossible-e11000-duplicate-key-error-dup-key-when-upserting)
