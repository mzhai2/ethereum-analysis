#!/bin/bash

# set the number of nodes
#SBATCH --nodes=1

# set the number of cores
#SBATCH --ntasks-per-node=1

#SBATCH --mem=20G

# set max wallclock time
#SBATCH --time=1-00:00:00

# set name of job
#SBATCH --job-name=create_db

# mail alert at start, end and abortion of execution
#SBATCH --mail-type=ALL

# send mail to this address
#SBATCH --mail-user=mzhai2@jhu.edu
ROOT=$HOME/Code/ethanalysis
EXP=$ROOT/ethanalysis/experiment1 
#SBATCH --output=$EXP/logs/%j.out
#SBATCH --error=$EXP/logs/%j.err

FULL=6900001
TEST=2000000
source activate eth

# start mongodb if its not running 
if pgrep -f mongod; then
    echo running;
else
    mongod -f $ROOT/configs/mongodb/mongodb-marcc.conf --fork
fi

# setup the experiment
python $EXP/experiment_setup.py eth 1 $TEST
killall -w mongod
